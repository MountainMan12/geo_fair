#from discoverpy import Discover
import sqlite3
import csv
import os
import json
import pandas as pd
import requests
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import seaborn as sns
import numpy as np
from functools import reduce
import math


def get_metrics():
    #metric dictionary linking the metadata labels that correspond to the evaluation metric
    metric_dict = {
        "F3" : ["Processed Data URI", "F3", 2],
        "F2" : ["Keywords describe study", "F2", 2],
        "I2" : ["Ontology Tags", "I2", 2],
        "R1_2_1" : ["Experiment Design", "R1", 1],
        "R1_2_2" : ["Contact Information", "R1", 0.5],
        "R1_2_3" : ["Publication Identifier", "R1", 0.5],
        "R1_2_4" : ["Platform ID", "R1", 1],
        "R1_2_5" : ["Counts Data", "R1", 2]  
    }
    return metric_dict

def F2(accession):
    F2_score = float(0)
    column = 'keyword'
    goal = 'gse'
    constraint = accession
    #(Meta)data contains keywords that describe the study
    #Check for the title and summary section for keywords
    c.execute("SELECT %s FROM bern_study_entities WHERE %s=?" % (column, goal), (constraint,))
    keywords = set(c.fetchall())
    for words in keywords: 
        if None in words:
            F2_score = 0
        else:
            F2_score = 1
    
    return F2_score


def F3(accession):
    #Raw data is not provided on discover
    F3_score = float(0)

    d_id = ''
    discover = Discover() 
    #GEO DATA LAKES
    discover.set_repo("9")
    
    #get platform id
    column = 'gpl'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM auto_groups WHERE %s=?" % (column, goal), (constraint,))
    plt_id = c.fetchall()
    
    for id in plt_id:
        plt = ''.join(id)
        d_id = str(accession+'_'+id)
    
    dataset_query_df = discover.dataset_repo.query_dataset_by_field("dataset_id", d_id)
    
    proc_file = str(dataset_query_df['_source.kw_key'])
    if proc_file:
        F3_score = 1
    else:
        F3_score = 0

    return(F3_score)

def I2(accession):
    #(Meta)data that are described by biological keywords are tagged with ontologies
    I2_score = float(0)
    ont_score = float(0) #Score per ontology tag
    column = 'ontology'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM bern_study_entities WHERE %s=?" % (column, goal), (constraint,))
    ont_labels = set(c.fetchall())
    
    if ont_labels:
        for label in ont_labels:
            label_str = ''.join(label)
            #Check whether the ontology tag exists in NCBO BioPortal
            page = requests.get('https://bioportal.bioontology.org/ontologies/'+label_str, allow_redirects = True)
            #print(label_str+':'+str(page.status_code))
            if(page.status_code == 200):
                ont_score += 1
    else:
        I2_score = 0
           
    if(ont_score > 0):
        I2_score = float(ont_score/len(ont_labels))
     
    return I2_score


def R1_2_1(accession):
    #Metadata provide information regarding the overall design of the experiment
    R1_2_1_score = float(0)
    column = 'overall_design'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM studies WHERE %s=?" % (column, goal), (constraint,))
    design = c.fetchall()
    
    for item in design: 
        if None in item:
            R1_2_1_score = 0
        else:
            R1_2_1_score = 1
        
    return R1_2_1_score


def R1_2_2(accession):
    #Metadata provides the contact information
    R1_2_2_score = float(0)

    page = requests.get('https://www.polly.ai/contact', allow_redirects = True)
    if(page.status_code == 200):
        R1_2_2_score = 1
    else:
        R1_2_2_score = 0
    
    return R1_2_2_score


def R1_2_3(accession):
    #Metadata specifies the publication identifier
    R1_2_3_score = float(0)
    column = 'pubmed_id'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM studies WHERE %s=?" % (column, goal), (constraint,))
    pub_id = c.fetchall()
    
    for pub in pub_id: 
        if None in pub:
            R1_2_3_score = 0
        else:
            R1_2_3_score = 1
        
    return R1_2_3_score


def R1_2_4(accession):
    #Metadata contains the platform ID 
    R1_2_4_score = float(0)
    column = 'gpl'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM auto_groups WHERE %s=?" % (column, goal), (constraint,))
    plt_id = c.fetchall()
    
    for plt in plt_id: 
        if None in plt:
            R1_2_4_score = 0
        else:
            R1_2_4_score = 1
        
    return R1_2_4_score

def R1_2_5(accession):
    #Counts data
    R1_2_5_score = int(1)
    
    return R1_2_5_score

def write_to_json(accession, scores):
    
    d_id = ''
    #get platform id
    column = 'gpl'
    goal = 'gse'
    constraint = accession
    
    c.execute("SELECT %s FROM auto_groups WHERE %s=?" % (column, goal), (constraint,))
    plt_id = c.fetchall()
    
    for id in plt_id:
        plt = ''.join(id)
        d_id = str(accession+'_'+plt)
    
    #data in JSON format
    data = {
            "dataset_id" : d_id,
            "metrics" :
                {
                    'F3' : float(scores[0]), 
                    'F2' : float(scores[1]),
                    'I2' : float(scores[2]),
                    'R1_2_1' : float(scores[3]),
                    'R1_2_2' : float(scores[4]),
                    'R1_2_3' : float(scores[5]),
                    'R1_2_4' : float(scores[6]),
                    'R1_2_5' : float(scores[7]),
                }
        }

    filename = "./json_dumps_datalake/"+d_id+".json"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as fh:
        json.dump(data, fh)
    
    fh.close()


def get_scores(accession):
    #Get Scores for each metric
    print('Performing evaluation for GSE ID: '+accession)
    sc1 = float(F3(accession))
    sc2 = float(F2(accession))
    sc3 = float(I2(accession))
    sc4 = float(R1_2_1(accession))
    sc5 = float(R1_2_2(accession))
    sc6 = float(R1_2_3(accession))
    sc7 = float(R1_2_4(accession))
    sc8 = float(R1_2_5(accession))
    
    scores = [sc1,sc2,sc3,sc4,sc5,sc6,sc7,sc8]

    #Calculate composite scores
    #composite_score(accession, scores)
    #Write scores to a JSON file
    #write_to_json(accession, scores)
    print('OK')

    return scores

def composite_score(accession, scores):
    m_dict = get_metrics()
    weights = []
    comp_score = float(0)
    per_score = float(0)
    max_score = int(0)

    for key, value in m_dict.items():
        weights.append(value[2])
    for i in range(0, len(scores)):
        comp_score = comp_score + weights[i]*scores[i]
        max_score = max_score + 1*weights[i]

    per_score = float(comp_score/max_score)*100
    
    filename = './bulk_eval/fair_score_polly_s2.csv'
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    with open(filename, 'a') as score:
        score.write(accession+','+str(per_score)+'\n')

    score.close()

def visualise_heatmap(accession, scores):

    m_dict = get_metrics()
    labels = [] #Metric description + FAIR guideline ID + Weights

    for key, value in m_dict.items():
        labels.append(value[0]+":"+value[1])

    scores = np.reshape(scores, (4,2))
    labels = np.reshape(labels, (4,2))


    #Generating heatmap for the evaluation
    filename = "./heatmaps/"+accession+'_datalake_eval.jpeg'
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    fig, ax = plt.subplots()
    sns.set(font_scale=0.75)
    ax = sns.heatmap(scores, annot=labels, fmt='', cmap = 'Purples',  linewidths=0.7, 
                     linecolor='white', yticklabels=False, xticklabels=False,
                     vmin = 0, vmax = 1)
    plt.title(accession+' Evaluation Scores (GEO Polly)')
    #Visualisation is stored in the current working directory
    plt.savefig(filename, dpi=300, bbox_inches="tight")
    print('Heatmap generated in current directory for: '+accession)

def create_score_csv(scores):
    
    # name of csv file  
    filename = "./bulk_eval/DL_GEO_scores.csv"
    os.makedirs(os.path.dirname(filename), exist_ok=True) 
    # writing to csv file  
    with open(filename, 'a') as csvfile:  
        # creating a csv writer object  
        csvwriter = csv.writer(csvfile)   
        # writing the data rows  
        csvwriter.writerow(scores) 

if __name__ == '__main__':
    
    conn = sqlite3.connect('GEOmetalake.db')
    c = conn.cursor()

    choice = int(input('Evaluation you would like to perform for GEO Data Lake Dataset?'+'\n'+'Enter 1 for Single and 0 for Bulk: '))
    if choice == int(0):
        #READ GSE IDs from File
        with open('geo_ids.txt', 'r') as f:
            for line in f:
                acc_id = line.strip()
                s = get_scores(acc_id)
                create_score_csv(s)
            print('CSV file generated for bulk evaluation')

    elif choice == int(1):
        acc_id = str(input('Please Enter a GSE ID: '))
        s = get_scores(acc_id)
        visualise_heatmap(acc_id, s)
    else:
        raise Exception('Inavlid entry, Try Again!')

