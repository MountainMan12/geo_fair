# FAIR Evaluation of NCBI-GEO Data #

Python code for evaluating a GEO Dataset on NCBI repository and assign FAIR scores for each metric. 

### INSTRUCTIONS ###
- Install required packages `pip3 install -r requirements.txt`
- For NCBI-GEO Dataset Evaluation: `python NCBI_GEO_eval.py`
- For GEO-DataLake evaluation: `python DL_GEO_eval.py`
- For Data Lake evaluation script `DL_GEO_eval.py`, the input database is available on drive. Rename the file to GEOmetalake.db and copy it to the current working directory

### WAYS TO PERFORM EVALUATION
- Users can perform evaluation for a single dataset by entering a valid GSE ID or use the geo_ids.txt file to perform evaluation for multiple studies.
- In case of single evaluation a heatmap will be generated
- In case of a bulk evaluation a CSV file will be generated where each row will contain the evaluations for each study
- In case of bulk evaluation, to Visualise frequencies of Pass and Fail studies, run: `python vis.py`.
	Takes `NCBI_GEO_scores.csv` and `DL_GEO_scores.csv` as input file which contains pre-evaluated, per metric scores for 100 studies and creates a visualisation in the current working directory.

### NOTE ###
- The GEO Metadata for the latest studies may not be available for download via GEOparse. 
  (Use study IDs that were released before September 2020 to avoid issues)
- Please contact if you do not have the .db file for Data Lake evaluation 

### CONSTRAINTS

- If a wrong GSE ID is entered, the script will not throw any exceptions.
- Does not check if the GSE ID is present in the database.

### Contact ###
Reach out to pawan.verma@elucidata.io for queries or conflicts

