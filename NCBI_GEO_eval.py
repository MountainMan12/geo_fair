import GEOparse
import json
import csv
from lxml import html
import os
from email_validator import validate_email, EmailNotValidError
import plotly.graph_objects as go
from sh import gunzip
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from functools import reduce

def get_geo(accession):
    #Checking if the GEO data already exists in unzipped SOFT format
    if os.path.exists('./metadata/'+accession+'_family.soft'):
        print("Metadata for "+accession+" exists, Performing evaluation...")
    else:
        #Metadata will be downloaded in the metadata folder on the current working directory
        print('Downloading GEO Metadata for GSE ID: '+accession+' in SOFT Format....')
        GEOparse.get_GEO(geo=accession, destdir="./metadata/", silent=True) 
        gunzip('./metadata/'+accession+'_family.soft.gz')
        print('Metadata Download and Extraction has finished for GSE ID: '+accession+', Performing evaluation...')

def get_metrics():
    #metric dictionary linking the metadata labels that correspond to the evaluation metric
    metric_dict = {
        "F3" : ["Processed Data URI", "F3", 2],
        "F2" : ["Keywords describe study", "F2", 2],
        "I2" : ["Ontology Tags", "I2", 2],
        "R1_2_1" : ["Experiment Design", "R1", 1],
        "R1_2_2" : ["Contact Information", "R1", 0.5],
        "R1_2_3" : ["Publication Identifier", "R1", 0.5],
        "R1_2_4" : ["Platform ID", "R1", 1],
        "R1_2_5" : ["Counts Data", "R1", 2]  
    }
    return metric_dict

def get_exp_type(accession):

    exp_type = ''

    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if('!Series_type' in line):
                exp_type = str(line.partition("=")[2]).strip()
                break
    f.close()
    return exp_type
    
def F3(accession):

    #Initialise evaluation scores
    F3_score = float(0)
    sample_files = []
    supp_file = ''
    if_file = int(0)
    if_sample_files = int(0)

    exp = get_exp_type(accession)
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if('array' in exp):
                if('!Sample_supplementary_file' in line): #Processed counts for microarray
                    sample_files.append(str(line.partition("=")[2]).strip())
                    if('NONE' not in sample_files):
                        if_sample_files = 1
                    else:
                        if_sample_files = 0
            elif('high throughput sequencing' in exp):
                #Processed data as raw/normalised counts 
                if('!Series_supplementary_file' in line):
                    supp_file = str(line.partition("=")[2]).strip()
                    if(not supp_file):
                        if_file = 0
                    else:
                        if_file = 1
                    
    f.close()

    if('array' in exp):
        F3_score = if_sample_files
    elif('high throughput sequencing' in exp):
        F3_score = if_file
    else:
        print('EXP NOT IDENTIFIED')
    
    return F3_score

def F2(accession):
    
    F2_score = float(0)
    meta_field = "!Series_summary = Keywords"
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                #Keywords present in the line
                keywords = line.partition(":")[2].split()
                if keywords:
                    F2_score = 1
                    break
                else:
                    F2_score = 0 #Keywords section empty
                    break
            else:
                F2_score = 0 #Keywords not provided
    f.close()
    return(F2_score)    

def I2(accession):
    
    I2_score = float(0)
    
    return(I2_score)
    
def R1_2_1(accession):
    
    R1_2_1_score = float(0)
    meta_field = "!Series_overall_design"
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                #Overall design of experiment is described
                design = str(line.partition("=")[2])
                if not design:
                    #empty string
                    R1_2_1_score = 0
                    break
                else:
                    #Design specified
                    R1_2_1_score = 1
                    break
            else:
                R1_2_1_score = 0
    f.close()  
    return R1_2_1_score

def R1_2_2(accession):
    
    R1_2_2_score = float(0)
    meta_field = "!Series_contact_email"
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                #We only look for an email to evaluate the metric 
                email = str(line.partition("=")[2]).strip()
                try:
                    #Validate the email ID
                    valid = validate_email(email)
                    R1_2_2_score = 1
                    break
                except EmailNotValidError as e:
                # email is not valid, exception message is human-readable
                        R1_2_2_score = 0
            else:
                #Contact Email not provided
                R1_2_2_score = 0
    f.close()
    return R1_2_2_score

def R1_2_3(accession):
    
    R1_2_3_score = float(0)
    meta_field = "!Series_pubmed_id"
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                pubmed_id = str(line.partition("=")[2])
                if not pubmed_id:
                    #Empty field
                    R1_2_3_score = 0
                    break
                else:
                    #Publication ID found
                    R1_2_3_score = 1
                    break
            else:
                #No publication ID provided
                R1_2_3_score = 0
    f.close()
    return R1_2_3_score
    
def R1_2_4(accession):
    
    R1_2_4_score = float(0)
    meta_field = "!Platform_geo_accession"
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                #Checking whether the Platform ID is made avaliable
                platform = str(line.partition("=")[2]).strip()
                if not platform:
                    #Platform ID is an empty field
                    R1_2_4_score = 0
                    break
                else:
                    #Platform ID avaliable
                    R1_2_4_score = 1
                    break
            else:
                #Platform ID not provided
                R1_2_4_score = 0
    return R1_2_4_score
 
def R1_2_5(accession):
    
    #Initialise evaluation scores
    R1_2_5_score = int(0)
    if_counts = int(0)

    exp = get_exp_type(accession)
    F3_eval = int(F3(accession)) #Is the file present ?

    file_types = ['count','counts','Count','Counts','RPKM','FPKM','TPM','FPKMs','RPKMs']
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        if('high throughput' in exp and F3_eval == 1):
            for line in f:
                if("!Sample_data_processing = Supplementary_files_format_and_content" in line):
                    file_type = str(line.partition(":")[2]).strip()
                    for type in file_types:
                        if(type in file_type):
                            if_counts = 1
                            break
                        else:
                            if_counts = 0
        elif('array' in exp and F3_eval == 1):
            if_counts = 1
        else:
            if_counts = 0

    f.close()
    R1_2_5_score = if_counts             
                          
    return R1_2_5_score
    
    
def get_scores(accession):
    #Get Scores for each metric
    sc1 = float(F3(accession))
    sc2 = float(F2(accession))
    sc3 = float(I2(accession))
    sc4 = float(R1_2_1(accession))
    sc5 = float(R1_2_2(accession))
    sc6 = float(R1_2_3(accession))
    sc7 = float(R1_2_4(accession))
    sc8 = float(R1_2_5(accession))
    
    scores = [sc1,sc2,sc3,sc4,sc5,sc6,sc7,sc8]

    #Write scores to a JSON file
    #composite_score(accession, scores)
    #write_to_json(accession, scores)
    print('OK')

    return scores

def write_to_json(accession, scores):

    meta_field = '!Platform_geo_accession'
    
    with open('./metadata/'+accession+'_family.soft', 'rt') as f:
        for line in f:
            if(meta_field in line):
                #Checking whether the Platform ID is made avaliable
                platform = str(line.partition("=")[2]).strip()
                break
    f.close()
    #data in JSON format
    data = {
            "dataset_id" : str(accession)+'_'+str(platform),
            "metrics" :
                {
                    'F3' : float(scores[0]), 
                    'F2' : float(scores[1]),
                    'I2' : float(scores[2]),
                    'R1_2_1' : float(scores[3]),
                    'R1_2_2' : float(scores[4]),
                    'R1_2_3' : float(scores[5]),
                    'R1_2_4' : float(scores[6]),
                    'R1_2_5' : float(scores[7]),
                }
        }

    filename = "./json_dumps_ncbi/"+accession+"_"+platform+".json"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as fh:
        json.dump(data, fh)
    
    fh.close()

def composite_score(accession, scores):
    m_dict = get_metrics()
    weights = []
    comp_score = float(0)
    per_score = float(0)
    max_score = int(0)

    for key, value in m_dict.items():
        weights.append(value[2])
    for i in range(0, len(scores)):
        comp_score = comp_score + weights[i]*scores[i]
        max_score = max_score + 1*weights[i]

    per_score = float(comp_score/max_score)*100
    
    filename = './bulk_eval/fair_score_ncbi_s2.csv'
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    with open(filename, 'a') as score:
        score.write(accession+','+str(per_score)+'\n')

    score.close()

def visualise_heatmap(accession, scores):

    m_dict = get_metrics()
    labels = [] #Metric description + FAIR guideline ID + Weights

    for key, value in m_dict.items():
        labels.append(value[0]+":"+value[1])

    scores = np.reshape(scores, (4,2))
    labels = np.reshape(labels, (4,2))

    filename = "./heatmaps/"+accession+'_ncbi_eval.jpeg'
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    #Generating heatmap for the evaluation
    fig, ax = plt.subplots()
    sns.set(font_scale=0.75)
    ax = sns.heatmap(scores, annot=labels, fmt='', cmap = 'Purples',  linewidths=0.5, 
                     linecolor='white', yticklabels=False, xticklabels=False,
                     vmin = 0, vmax = 1)
    plt.title(accession+' Evaluation Scores (GEO-NCBI)')
    #Visualisation is stored in the heatmap directory
    plt.savefig(filename, dpi=300, bbox_inches="tight")
    

def create_score_csv(scores):
    
    # name of csv file
    filename = "./bulk_eval/NCBI_GEO_scores.csv"
    os.makedirs(os.path.dirname(filename), exist_ok=True)  
    # writing to csv file  
    with open(filename, 'a') as csvfile:  
        # creating a csv writer object  
        csvwriter = csv.writer(csvfile)   
        # writing the data rows  
        csvwriter.writerow(scores) 

if __name__ == '__main__':

    choice = int(input('Evaluation you would like to perform for GEO Data Lake Dataset?'+'\n'+'Enter 1 for Single and 0 for Bulk: '))
    if choice == int(0):
        #READ GSE IDs from File
        with open('geo_ids2.txt', 'r') as f:
            for line in f:
                acc_id = line.strip()
                get_geo(acc_id)
                s = get_scores(acc_id)
                create_score_csv(s)
            print('CSV file generated for bulk evaluation')

    elif choice == int(1):
        acc_id = str(input('Please Enter a GSE ID: '))
        get_geo(acc_id)
        s = get_scores(acc_id)
        visualise_heatmap(acc_id, s)
    else:
        raise Exception('Inavlid entry, Try Again!')

