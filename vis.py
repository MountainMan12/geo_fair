import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

gse_id = []

with open('geo_ids.txt', 'r') as f:
    for line in f:
        gse_id.append(line.strip())

def DL_vis():

    df = pd.read_csv('./bulk_eval/DL_GEO_scores.csv', header=None)
    df.columns = ['F3: Processed Data URI', 'F2: Keywords descrive study', 'I2: Ontology Tags', 'R1.2.1: Experiment Design', 'R1.2.2: Contact Information', 'R1.2.3: Publication Identifier', 'R1.2.4: Platform ID', 'R1.2.5: Counts Data']
    #Replace 1.0 with PASS and 0 with FAIL in dataframe
    for col in df.columns:
        df.loc[df[col].between(0,1, inclusive=False), col] = 'Satisfactory'
    df_new = df.replace([1.0], 'Pass')
    df_new = df_new.replace([0.0], 'Fail')
    

    df_plot = pd.melt(df_new)
    sns.countplot(data=df_plot, x='variable', hue='value')
    plt.xlabel('Metric ID')
    plt.legend(loc='upper right')
    plt.xticks(rotation=90)
    plt.title('Pass/Fail studies for FAIR metric (GEO Polly)')
    plt.savefig('Metric_DL.jpeg', bbox_inches = 'tight')

def NCBI_vis():

    df = pd.read_csv('./bulk_eval/NCBI_GEO_scores.csv', header=None)
    df.columns = ['F3: Processed Data URI', 'F2: Keywords descrive study', 'I2: Ontology Tags', 'R1.2.1: Experiment Design', 'R1.2.2: Contact Information', 'R1.2.3: Publication Identifier', 'R1.2.4: Platform ID', 'R1.2.5: Counts Data']
    #Replace 1.0 with PASS and 0 with FAIL in dataframe
    for col in df.columns:
        df.loc[df[col].between(0,1, inclusive=False), col] = 'Satisfactory'
    df_new = df.replace([1.0], 'Pass')
    df_new = df_new.replace([0.0], 'Fail')
    df_plot = pd.melt(df_new)
    sns.countplot(data=df_plot, x='variable', hue='value')
    plt.xlabel('Metric ID')
    plt.xticks(rotation=90)
    plt.legend(loc='upper right')
    plt.title('Pass/Fail studies for FAIR metric (GEO NCBI)')
    plt.savefig('Metric_NCBI.jpeg', bbox_inches = 'tight')
    
if __name__ == '__main__':

    choice = int(input("Plot evaluation frequencies for each metric, Dataset?"+"\n"+"Enter 1 for GEO Polly or 2 for GEO NCBI: "))
    if choice == int(1):
        DL_vis()
    elif choice == int(2):
        NCBI_vis()
    else:
        raise Exception('Inavlid entry, Try Again!')

